
class Order{
  final int id;
  final String name;
  final String status;
  final int total;
  final int amount;
  final int date;
  
  Order({this.id,this.name,this.status,this.total,this.amount,this.date});

  factory Order.internalFromJson(Map<String,dynamic> json){
    return Order(
      id: json["id"] as int,
      name: json["name"] as String,
      status: json["status"] as String,
      total: json["total"] as int,
      amount: json["amount"] as int,
      date: json["date"] as int,
    );
  }
}