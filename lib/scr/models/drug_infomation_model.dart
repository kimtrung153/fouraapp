class DrugInfo {
  final int id;
  final String title;
  final String alcCode;
  final int workPlaceID;
  final String workPlaceName;
  final String price;
  final int amount;
  final int exprireDate; 
  final String name;
  final String email;
  final String phone;
  DrugInfo(
      {this.id,
      this.title,
      this.alcCode,
      this.workPlaceID,
      this.workPlaceName,
      this.price,
      this.amount,
      this.exprireDate,
      this.email,
      this.name,
      this.phone});
  factory DrugInfo.internalFromJson(Map<String, dynamic> json) {
    return DrugInfo(
      id: json["id"] as int,
      title: json["title"] as String,
      alcCode: json["alc_code"] as String,
      workPlaceID: json["workplace_id"] as int,
      workPlaceName: json["workplace_name"] as String,
      price: json["price"] as String,
      amount: json["amount"] as int,
      exprireDate: json["expire_date"] as int,
      name: json["provider"]["name"] as String,
      email: json["provider"]["email"] as String,
      phone: json["provider"]["phone"] as String,
    );
  }
}
