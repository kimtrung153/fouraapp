class Workplace{
  final int id;
  final String name;
  Workplace({this.id,this.name});

  factory Workplace.internalFromJson(Map<String,dynamic>json){
    return Workplace(
      id: json["id"] as int,
      name: json["name"] as String,
    );
  }
}