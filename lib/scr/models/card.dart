class CardInfo {
  final int id;
  final String name;
  final int userID;
  final String stripeCardID;
  final String cardBrand;
  final String cardLastFour;
  final int expMonth;
  final int expYear;

  bool isChosen;
  
  CardInfo({
    this.id,
    this.name,
    this.userID,
    this.stripeCardID,
    this.cardBrand,
    this.cardLastFour,
    this.expMonth,
    this.expYear,
    this.isChosen,
  });
  factory CardInfo.internalFromJson(Map<String, dynamic> json){
    return CardInfo(
      id: json["id"] as int,
      name: json["name"] as String,
      userID: json["user_id"] as int,
      stripeCardID: json["stripe_card_id"] as String,
      cardBrand: json["card_brand"] as String,
      cardLastFour: json["card_last_four"] as String,
      expMonth: json["exp_month"] as int,
      expYear: json["exp_year"] as int,
      isChosen: false,
    );
  }
}