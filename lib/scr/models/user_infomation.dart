
import 'package:foura_app/scr/models/subscriptions.dart';

class UserInfo {
  final String userName;
  final String phoneNumber;
  final String firstName;
  final String lastName;
  final String email;
  final String company;
  final String street;
  final String city;
  final String country;
  final String postalCode;
  final List<Subscription> subscription;

  UserInfo(
      {this.city,
      this.company,
      this.country,
      this.email,
      this.firstName,
      this.lastName,
      this.phoneNumber,
      this.postalCode,
      this.street,
      this.userName,
      this.subscription
      });

  factory UserInfo.internalFromJson(Map<String,dynamic> json){
    return UserInfo(
      city: json["city"] as String,
      company: json["company"] as String,
      country: json["country"] as String,
      email: json["email"] as String,
      firstName: json["first_name"] as String,
      lastName: json["last_name"] as String,
      phoneNumber: json["phone_number"] as String,
      postalCode: json["postal_code"] as String,
      street: json["street"] as String,
      userName: json["username"] as String,
      subscription: json["subscription"] != null
         ? ((json["subscription"]) as List<dynamic>)
             .cast<Map<String, dynamic>>()
             .map<Subscription>(
                 (value) => Subscription.internalFromJson(value))
             .toList()
         : [],
    );
  }
}
