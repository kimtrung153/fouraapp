class Subscription{
  final int id;
  final int product;
  final String productName;
  final String status;
  final int nextPaymentDate;
  final int total;

  Subscription({this.id,this.nextPaymentDate,this.product,this.productName,this.status,this.total});

  factory Subscription.internalFromJson(Map<String,dynamic> json ){
    return Subscription(
      id: json["id"] as int,
      nextPaymentDate: json["next_payment_date"] as int,
      product: json["product"] as int,
      productName: json["product_name"] as String,
      status: json["status"] as String,
      total: json["total"] as int,
    );
  }
}