import 'package:http/http.dart' as http;
import 'dart:convert';

class Authentication {
  //LOGIN PROCESSING
  Future<dynamic> login(String email, String password) async {
    var response =
        await http.post("http://foura.stdiohue.com/api/auth/login", body: {
      "email": email,
      "password": password,
    });
    if (response.statusCode == 200) {
      return response;
    } else
      return json.decode(response.body)["message"];
  }

  //SIGN UP PROCESSING
  Future<dynamic> signUp(String userName, String email, String password) async {
    var response =
        await http.post("http://foura.stdiohue.com/api/auth/signup", body: {
      "email": email,
      "username": userName,
      "password": password,
    });
    if (response.statusCode == 200) {
      return response;
    } else
      return json.decode(response.body)["message"];
  }
}
