import 'package:foura_app/scr/models/card.dart';
import 'package:foura_app/scr/models/drug_infomation_model.dart';
import 'package:foura_app/scr/models/order.dart';
import 'package:foura_app/scr/models/user_infomation.dart';
import 'package:foura_app/scr/models/workplace.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Api {
  //GET USER INFORMATION

  Future<UserInfo> getUserInformation(String accessToken) async {
    final response = await http.post("http://foura.stdiohue.com/api/auth/me",
        headers: {"Authorization": "Bearer $accessToken"});
    if (response.statusCode == 200) {
      print(json.decode(response.body)["message"]);
      final parseCampus =
          UserInfo.internalFromJson(json.decode(response.body)["data"]);
      return parseCampus;
    } else
      throw Exception("fail to load data");
  }

  //GET LIST DRUG THAT APPEAR ON MEDICAL SCREEN

  Future<List<DrugInfo>> getListDrug() async {
    var response = await http.post(
      "http://foura.stdiohue.com/api/ad/get-list?page=1&limit=50",
    );
    print(json.decode(response.body)["message"]);
    if (response.statusCode == 200) {
      final parseCampus = json
          .decode(response.body)["data"]["data"]
          .cast<Map<String, dynamic>>();
      return parseCampus
          .map<DrugInfo>((value) => DrugInfo.internalFromJson(value))
          .toList();
    } else
      throw Exception("fail to load data");
  }

  //GET DRUG'S INFORMATION

  Future<DrugInfo> getDrugInfo(int id) async {
    final response = await http.get(
      "http://foura.stdiohue.com/api/ad/view-detail?id=$id",
    );
    if (response.statusCode == 200) {
      print(json.decode(response.body)["message"]);
      final parseCampus =
          DrugInfo.internalFromJson(json.decode(response.body)["data"]);
      return parseCampus;
    } else
      throw Exception("fail to load data");
  }

  // GET STRIPE CARD'S INFORMATION

  Future<List<CardInfo>> getCardData(String accessToken) async {
    var response = await http.get(
        "http://foura.stdiohue.com/api/payment/view-cards",
        headers: {"Authorization": "Bearer $accessToken"});
    print(json.decode(response.body)["message"]);
    if (response.statusCode == 200) {
      final parseCampus =
          json.decode(response.body)["data"].cast<Map<String, dynamic>>();
      return parseCampus
          .map<CardInfo>((value) => CardInfo.internalFromJson(value))
          .toList();
    } else
      throw Exception("fail to load data");
  }

  // CREATE ADVERTISEMENT

  Future<http.Response> postcreateAD(
    String accessToken,
    String title,
    String alcCode,
    String amount,
    String price,
    String workPlaceID,
    String exprireDate,
  ) async {
    var response =
        await http.post("http://foura.stdiohue.com/api/ad/create-ad", headers: {
      "Authorization": "Bearer $accessToken"
    }, body: {
      "title": title,
      "alc_code": alcCode,
      "quantity": amount,
      "price": price,
      "expire_date": exprireDate,
      "workplace_id": workPlaceID,
    });
    return response;
  }

// CHANGE USER'S ADDRESS

  Future<http.Response> postChangeAddress(
      String accessToken,
      String firstName,
      String lastName,
      String company,
      String street,
      String postalCode,
      String country,
      String city,
      String phone) async {
    var response = await http
        .post("http://foura.stdiohue.com/api/payment/changeAddress", headers: {
      "Authorization": "Bearer $accessToken"
    }, body: {
      "country": country,
      "first_name": firstName,
      "last_name": lastName,
      "company": company,
      "street": street,
      "postal_code": postalCode,
      "city": city,
      "phone": phone,
    });
    return response;
  }

//

  Future<List<Workplace>> postListWorkplace() async {
    final response = await http.post(
      "http://foura.stdiohue.com/api/ad/get-list-workplace",
    );
    final parseCampus =
        json.decode(response.body)["data"].cast<Map<String, dynamic>>();
    return parseCampus
        .map<Workplace>((value) => Workplace.internalFromJson(value))
        .toList();
  }

  //ADD STRIPE CARD

  Future<http.Response> postAddCard(
      String accessToken, String stripeCardToken) async {
    var response = await http.post(
        "http://foura.stdiohue.com/api/payment/add-card-via-token",
        headers: {
          "Authorization": "Bearer $accessToken",
        },
        body: {
          "stripe_card_token": stripeCardToken,
        });
    return response;
  }

  // GET LIST ORDER

  Future<List<Order>> getListOrder(String accessToken) async {
    var response = await http.get(
        "http://foura.stdiohue.com/api/payment/view-list-orders",
        headers: {
          "Authorization": "Bearer $accessToken",
        });
    if (response.statusCode == 200) {
      print(json.decode(response.body)["message"]);
      final parseCampus = json
          .decode(response.body)["data"]["data"]
          .cast<Map<String, dynamic>>();
      return parseCampus
          .map<Order>((value) => Order.internalFromJson(value))
          .toList();
    } else
      throw Exception("fail to load data");
  }
}
