import 'package:flutter/material.dart';
import 'package:foura_app/scr/ui/sign_screens/sign_in_page.dart';
import 'package:foura_app/scr/ui/sign_screens/sign_up_page.dart';

class FouraScreen extends StatefulWidget {
  @override
  _FouraScreenState createState() => _FouraScreenState();
}

class _FouraScreenState extends State<FouraScreen> {
  @override
  Widget build(BuildContext context) {
    Color _buttonColor = Colors.white;
    TextStyle _buttonStyle = TextStyle(
        color: Colors.blue, fontSize: 16, fontWeight: FontWeight.bold);
    return Scaffold(
      backgroundColor: Colors.blue,
      body: ListView(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 80.0),
            child: Image.asset('images/Foura_Logo.png', fit: BoxFit.cover),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 200, 20, 15),
            child: RaisedButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => SignUpPage()));
              },
              color: _buttonColor,
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Text(
                'Sign Up',
                style: _buttonStyle,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: RaisedButton(
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => SignInPage()),
                );
              },
              color: _buttonColor,
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Text(
                'Sign In',
                style: _buttonStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
