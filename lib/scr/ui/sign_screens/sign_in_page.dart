import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'package:foura_app/scr/api/authentication.dart';
import 'package:foura_app/scr/ui/home_screens/home_screen.dart';
import 'package:foura_app/scr/ui/sign_screens/sign_up_page.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Authentication authentication = Provider.of<Authentication>(context);
    Api api = Provider.of<Api>(context);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.blue,
      body: ListView(
        children: <Widget>[
          Container(
            height: 135,
            margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 80),
            child: Image.asset("images/Foura_Logo.png"),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
              controller: _emailController,
              cursorColor: Colors.white,
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2)),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1)),
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                  hintText: "email",
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300)),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
              controller: _passwordController,
              cursorColor: Colors.white,
              obscureText: true,
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2)),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1)),
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.white,
                  ),
                  hintText: "password",
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300)),
            ),
          ),
          SizedBox(
            height: 80,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30),
            child: RaisedButton(
              onPressed: () {
                authentication
                    .login(_emailController.text, _passwordController.text)
                    .then((confirmObject) {
                  if (confirmObject is Response) {
                    print(json.decode(confirmObject.body)["data"]
                        ["access_token"]);
                   _scaffoldKey.currentState.showSnackBar(SnackBar(content: Row(
                     mainAxisSize: MainAxisSize.min,
                     children: <Widget>[
                       CircularProgressIndicator(),
                       SizedBox(width: 8,),
                       Text("Loading..")
                     ],
                   ),));
                    api
                        .getUserInformation(json
                            .decode(confirmObject.body)["data"]["access_token"])
                        .then((userInfo) {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (_) => MainScreen(
                                userInfo: userInfo,
                                accessToken:
                                    json.decode(confirmObject.body)["data"]
                                        ["access_token"]),
                          ));
                    });
                  } else
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(confirmObject),
                    ));
                });
              },
              elevation: 5.0,
              padding: const EdgeInsets.symmetric(vertical: 13),
              color: Colors.white,
              child: Text(
                "Sign In",
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Don't have account?",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w400),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushReplacement(
                      context, MaterialPageRoute(builder: (_) => SignUpPage()));
                },
                splashColor: Colors.transparent,
                child: Text(
                  "Sign Up",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
