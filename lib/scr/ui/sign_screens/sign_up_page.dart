import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/authentication.dart';
import 'package:foura_app/scr/ui/home_screens/home_screen.dart';
import 'package:foura_app/scr/ui/sign_screens/sign_in_page.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool acceptanceVal = false;
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Authentication authentication = Provider.of<Authentication>(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.blue,
      body: ListView(
        children: <Widget>[
          Container(
            height: 135,
            margin: const EdgeInsets.fromLTRB(50, 80, 50, 20),
            child: Image.asset("images/Foura_Logo.png"),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
              controller: _nameController,
              cursorColor: Colors.white,
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2)),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1)),
                  prefixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.white,
                  ),
                  hintText: "Name",
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300)),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
              controller: _emailController,
              cursorColor: Colors.white,
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2)),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1)),
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                  hintText: "email",
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300)),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
              controller: _passwordController,
              cursorColor: Colors.white,
              obscureText: true,
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2)),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1)),
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.white,
                  ),
                  hintText: "password",
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300)),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(15, 10, 15, 0),
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: acceptanceVal,
                  activeColor: Colors.white,
                  checkColor: Colors.blue,
                  onChanged: (bool val) {
                    setState(() {
                      acceptanceVal = val;
                    });
                  },
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 100,
                  child: Text(
                    """J'accepte les termes du système et la politique de confidentialité de Foura""",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 55,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30),
            child: RaisedButton(
              onPressed: () {
                if (acceptanceVal == false) {
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content:
                          Text("You must accept the terms of service first")));
                } else {
                  authentication
                      .signUp(_nameController.text, _emailController.text,
                          _passwordController.text)
                      .then((confirmObject) {
                    if (confirmObject is Response) {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(
                              json.decode(confirmObject.body)["message"])));
                      Timer(Duration(seconds: 3), () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (_) => MainScreen()));
                      });
                    } else
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text(confirmObject),
                      ));
                  });
                }
              },
              elevation: 5.0,
              color: Colors.white,
              padding: const EdgeInsets.symmetric(vertical: 13),
              child: Text(
                "Sign Up",
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Already have account? ",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w400),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushReplacement(
                      context, MaterialPageRoute(builder: (_) => SignInPage()));
                },
                splashColor: Colors.transparent,
                child: Text(
                  "Sign In",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
