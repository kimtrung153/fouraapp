import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'after_delete_passing_ad.dart';

//EDIT INFORMATION OF PASSING DRUG

class EditPassingInformation extends StatefulWidget {
  @override
  _EditPassingInformationState createState() => _EditPassingInformationState();
}

class _EditPassingInformationState extends State<EditPassingInformation> {
  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            content: Text(
              'Voulez-vous vous désabonner?',
              style: TextStyle(
                color: Colors.black,
                fontSize: 17,
                fontWeight: FontWeight.w400,
              ),
            ),
            actions: <Widget>[
              Container(
                child: FlatButton(
                  child: Text(
                    'Annuler',
                    style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.w600,
                        fontSize: 17),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              FlatButton(
                child: Text(
                  'D\'accord',
                  style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                      fontSize: 17),
                ),
                onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AfterDelPassingAD()));
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Novalgin',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          //_container CONTAIN EACH TYPE OF INFORMATION OF DRUG 
          _container('Novalgin',),
          _container('3413257',),
          _container('1',),
          _container('3.5',),
          _row('31/05/2019',),
          _row('Cher',),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              height: 50,
              margin: const EdgeInsets.fromLTRB(40, 60, 40, 0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
              child: Text(
                'Modifier l\'annonce',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              _showDialog();
            },
            child: Container(
              height: 50,
              margin: const EdgeInsets.fromLTRB(40, 20, 40, 50),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.red)),
              alignment: Alignment.center,
              child: Text(
                'Effacer',
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Container _container(String txt) => Container(
      height: 50,
      margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
      padding: const EdgeInsets.only(left: 15.5),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(5),
      ),
      alignment: Alignment.centerLeft,
      child: Text(
        txt,
        style: TextStyle(
            color: Colors.black, fontSize: 14, fontWeight: FontWeight.w400),
      ),
    );

Container _row(String txt,) => Container(
      height: 50,
      margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
      padding: const EdgeInsets.only(left: 15.5, right: 15.5),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(5),
      ),
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            txt,
            style: TextStyle(
                color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w400),
          ),
          Icon(
            Icons.keyboard_arrow_down,
            color: Colors.grey,
            size: 22,
          ),
        ],
      ),
    );

// backgroundColor: Colors.white,
// shape: RoundedRectangleBorder(
//     borderRadius: BorderRadius.all(Radius.circular(20))),
// contentPadding: const EdgeInsets.only(top: 19),
// content: Container(
//   height: 130,
//   width: 270,
//   child: Column(
//     children: <Widget>[
//       Container(
//         alignment: Alignment.topCenter,
//         padding: const EdgeInsets.only(bottom: 45),
//         child: Text(
//           'Voulez-vous vous désabonner?',
//           style: TextStyle(
//               color: Colors.black,
//               fontSize: 16,
//               fontWeight: FontWeight.w400),
//         ),
//       ),
//       Container(
//         decoration: BoxDecoration(
//             border: Border(
//                 top: BorderSide(color: Colors.blue, width: 0.8))),
//         child: Row(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             GestureDetector(
//               onTap: () {},
//               child: Container(
//                 width: 135,
//                 decoration: BoxDecoration(
//                   color: Colors.amber,
//                   border: Border(right: BorderSide(color: Colors.blue,width: 0.8))
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   'Annuler',
//                   style: TextStyle(
//                       color: Colors.blue,
//                       fontWeight: FontWeight.w500,
//                       fontSize: 17),
//                 ),
//               ),
//             ),
//             GestureDetector(
//               onTap: () {},
//               child: Container(
//                 width: 140,
//                 height: 44,
//                 alignment: Alignment.center,
//                 child: Text(

//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ],
//   ),
// ),
