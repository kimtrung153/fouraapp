import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foura_app/scr/ui/tab1/confirmation_order.dart';


//SUGGEST USER POST AN ADVERTISEMENT AFTER SHOW MANUAL INSTRUCTIONS

class SuggestUserPostAD extends StatefulWidget {
  @override
  _SuggestUserPostADState createState() => _SuggestUserPostADState();
}

class _SuggestUserPostADState extends State<SuggestUserPostAD> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: Text(
            'Petites Annonces',
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(15),
        children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
            child: Text(
              'S’abonner',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10,),
          Text(
            '''Désolé, vous devez être un abonné actif pour avoir accès.''',
            style: TextStyle(
                color: Colors.grey,
                fontSize: 14,
                fontWeight: FontWeight.w500),
          ),
          Text('Abonnement 3 mois à',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w500)),
          SizedBox(height: 10,),
          Text(
            '''0,00 € HT / mois pour 3 mois avec 3 mois d’essai gratuit

L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.

Certaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.

Testez gratuitement pendant 3 mois notre service : vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.''',
            style: TextStyle(
                color: Colors.grey,
                fontSize: 14,
                fontWeight: FontWeight.w500),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 70, 40, 0),
            child: RaisedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (_)=> ConfirmationOrder()));
              },
              color: Colors.blue,
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                "S\'abonner",
                style: TextStyle(color: Colors.white,fontSize: 16,fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
