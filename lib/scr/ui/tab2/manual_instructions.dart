import 'package:flutter/material.dart';

//SHOW MANUAL INSTRUCTIONS

class InstructionScreen extends StatefulWidget {
  @override
  _InstructionScreenState createState() => _InstructionScreenState();
}

class _InstructionScreenState extends State<InstructionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Créer une liste de produits',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
        child: ListView(
          children: <Widget>[
            Container(
              height: 38,
              child: Text(
                'Comment créer une liste de médicaments bientôt périmés ?',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              height: 30,
              margin: const EdgeInsets.only(top: 20),
              child: Text(
                'LGPI',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              height: 384,
              margin: const EdgeInsets.only(top: 10),
              child: Text(
                '''Pour établir une liste de produits susceptibles de périmer voilà comment procéder sur LGPI.

Tapez G (utilitaires / listes) puis tapez J (logiciel) puis tapez N (codification produits).

Faire F9 (critères)

Dans la colonne « Stock » sélectionnez la case Stock Total et sélectionnez la ligne « est supérieur à » puis inscrivez « 0 ».

Dans la colonne « Généralités » sélectionnez la case « date de dernière vente » et sélectionnez « est inférieur à » et inscrivez une date (18 à 24 mois antérieur à la date du jour).

Dans la case « Gestion » sélectionnez la case « prix de vente » et sélectionnez « est supérieur à » et inscrivez un prix (supérieur à 30 € pour limiter la liste)

Voilà il ne vous reste plus qu’à vérifier la date de péremption.''',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              height: 30,
              margin: const EdgeInsets.only(top: 28),
              child: Text('WINPHARMA',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500)),
            ),
            Container(
              height: 256,
              margin: const EdgeInsets.only(top: 6),
              child: Text(
                '''Allez dans « Stock », « État du stock », « Voir le stock… »

Sélectionnez « Détail », « Nom », « Sans Groupe »

Décochez « Tous les produits en stock »

Les produits sans vente : rentrer le « nombre » de jours

Remplir ou non des éventuels filtres : TVA…

(Vous pouvez contacter l’assistance afin de mettre en place un filtre prix.)

Pour imprimer la liste : « Écran », « Imprimer »''',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              height: 30,
              margin: const EdgeInsets.only(top: 26),
              child: Text('ALLIADIS',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500)),
            ),
            Container(
              height: 320,
              margin: const EdgeInsets.only(top: 6),
              child: Text(
                '''Procédure pour sortir une liste de produits, allez dans Fichiers > Produits > Produits (et entrée).

Appuyez sur F1 : liste puis Modification des paramétres.

Faites Impression: imprimante ou écran > Format: liste produit (et non liste des produits) > Classement: alphabétique > Sélection: oui.

32: date de péremption; périmé entre : choisir les mois

33: données stock: réel supérieur à zéro

03 : prix public mini ( si on veut se fixer un prix )

06: TVA (si on veut lister par TVA)

Entrée jusqu’à impression ou affichage''',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
