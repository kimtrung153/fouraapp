import 'package:flutter/material.dart';
import 'package:foura_app/scr/ui/home_screens/home_screen.dart';

// SCREEN AFTER DELETING A PASSING ADVERTISEMENT

class AfterDelPassingAD extends StatefulWidget {
  @override
  _AfterDelPassingADState createState() => _AfterDelPassingADState();
}

class _AfterDelPassingADState extends State<AfterDelPassingAD> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back,),
            onPressed: (){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MainScreen()));
            },
          ),
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Mes annonces',
            style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }
}