import 'package:flutter/material.dart';

//SHOW USER'S PAYING ADDRESS

class ShowAddressScreen extends StatefulWidget {
  @override
  _ShowAddressScreenState createState() => _ShowAddressScreenState();
}

class _ShowAddressScreenState extends State<ShowAddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          title: Text(
            'Modifier mon adresse',
            style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: Container(
        height: 158,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.fromLTRB(16, 10, 12,9),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey,width: 0.8)),
              ),
              child: Text(
                '''Les adresses suivantes seront utilisées par défaut sur la page de commande.''',
                style: TextStyle(color: Colors.grey,fontSize: 13,fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 12, 16, 12),
              child: GestureDetector(
                onTap: (){},
                              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'images/locationicon.png',
                      width: 22,
                      height: 22,
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 13.5),
                      width: 120,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              'STDIOHUE',
                              style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              'Joanne Jemison',
                              style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              '37 Rue de Rivoli',
                              style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w400),
                            ),
                          ),
                          Text(
                            '75004 PARIS',
                            style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 80,
                      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width-208),
                      alignment: Alignment.center,
                      child: Icon(Icons.arrow_forward_ios,size: 22,color: Colors.grey,),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}