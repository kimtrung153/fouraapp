import 'package:flutter/material.dart';

//SHOW ALL ADVERTISEMENT OF USER (BOTH PASSING AND BUYING)
class AllAdvertisement extends StatefulWidget {
  @override
  _AllAdvertisementState createState() => _AllAdvertisementState();
}

class _AllAdvertisementState extends State<AllAdvertisement> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Mes annonces',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 20, left: 16),
        child: Text(
          'Vous n\'avez encore publié aucune annonce.',
          style: TextStyle(
              color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w400),
        ),
      ),
    );
  }
}
