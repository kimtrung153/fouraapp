import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'package:foura_app/scr/models/card.dart';
import 'add_payment_method.dart';

//USER ADD BANK FOR PAYMENT HERE

class AddBankScreen extends StatefulWidget {
  final String accessToken;
  AddBankScreen({this.accessToken});
  @override
  _AddBankScreenState createState() => _AddBankScreenState();
}

class _AddBankScreenState extends State<AddBankScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _showListCard = true;
  List<CardInfo> _listCard = [];
  @override
  void initState() {
    Api().getCardData(widget.accessToken).then((listCard){
      setState(() {
        _listCard = listCard;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          title: Text(
            'Moyens de paiement',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 40,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(bottom: BorderSide(color: Colors.grey, width: 1)),
            ),
            child: Text(
              "Aucun moyen sauvegardé trouvé.",
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            margin: const EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'images/credit.png',
                        height: 11,
                        width: 14,
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Carte de crédit ou de débit",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "*1220",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    IconButton(
                      splashColor: Colors.grey[50],
                      onPressed: () {
                        setState(() {
                          _showListCard = !_showListCard;
                        });
                      },
                      icon: Icon(
                        _showListCard
                            ? Icons.keyboard_arrow_down
                            : Icons.keyboard_arrow_right,
                        size: 25,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          _showListCard == false
              ? Container()
              : SingleChildScrollView(
                  child: Column(
                      children: _listCard.map((card) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          card.isChosen = !card.isChosen;
                        });
                      },
                      child: Container(
                        margin: const EdgeInsets.only(
                            bottom: 8, left: 50, right: 15),
                        padding: const EdgeInsets.only(bottom: 5),
                        decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  height: 28,
                                  width: 48,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        color: Colors.grey, width: 1),
                                  ),
                                  child: Image.asset(
                                    card.cardBrand == "Visa"
                                        ? "images/visa_1.png"
                                        : "images/normalcard.png",
                                    height: 9.1,
                                    width: 30,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Text(
                                  "*${card.cardLastFour}",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400),
                                ),
                              ],
                            ),
                            Icon(
                              Icons.check,
                              color: card.isChosen == true
                                  ? Colors.red
                                  : Colors.white,
                            ),
                          ],
                        ),
                      ),
                    );
                  }).toList()),
                ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (_)=>AddPaymentMethod(accesstoken: widget.accessToken,)));
        },
        icon: Icon(Icons.payment,color: Colors.white,size: 25,),
        label: Text("Add Card",style: TextStyle(color: Colors.white,fontSize: 13,fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
