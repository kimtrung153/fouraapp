import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'package:foura_app/scr/models/order.dart';

class OrdersOfUser extends StatefulWidget {
  final String accessToken;
  OrdersOfUser({this.accessToken});
  @override
  _OrdersOfUserState createState() => _OrdersOfUserState();
}

class _OrdersOfUserState extends State<OrdersOfUser> {
  ListView _noneOrder() => ListView(
        padding: const EdgeInsets.only(top: 20, left: 16, bottom: 62),
        children: <Widget>[
          Text(
            "Aucune commande n’a encore été passée.",
            style: TextStyle(
                color: Colors.black, fontSize: 14, fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 400,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: RaisedButton(
              onPressed: () {},
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                "Aller à la boutique",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              color: Colors.blue,
              elevation: 5,
            ),
          ),
        ],
      );
  ListView _showListOrder() => ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        itemCount: _listOrder.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {},
            child: Container(
              height: 130,
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.all(18),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 2),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                            text: "Commande: ",
                            style: _elementStyle,
                            children: <TextSpan>[
                              TextSpan(
                                  text: _listOrder[index].name,
                                  style: _orderStyle)
                            ]),
                      ),
                      RichText(
                        text: TextSpan(
                            text: "Date: ",
                            style: _elementStyle,
                            children: <TextSpan>[
                              TextSpan(
                                  text: DateTime.fromMillisecondsSinceEpoch(
                                          _listOrder[index].date * 1000)
                                      .toString()
                                      .substring(0, 10),
                                  style: _orderStyle)
                            ]),
                      ),
                      RichText(
                        text: TextSpan(
                            text: "État: ",
                            style: _elementStyle,
                            children: <TextSpan>[
                              TextSpan(
                                  text: _listOrder[index].status,
                                  style: _orderStyle)
                            ]),
                      ),
                      RichText(
                        text: TextSpan(
                            text: "Total: ",
                            style: _elementStyle,
                            children: <TextSpan>[
                              TextSpan(
                                  text:
                                      "${_listOrder[index].total}€ pour 1 article",
                                  style: _orderStyle)
                            ]),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        height: 35,
                        child: RaisedButton(
                          onPressed: () {},
                          color: Colors.blue,
                          child: Text(
                            "Voir",
                            style: _buttonTextStyle,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                        child: RaisedButton(
                          onPressed: () {},
                          color: Colors.blue,
                          child: Text(
                            "Voir la facture",
                            style: _buttonTextStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );
  List<Order> _listOrder = [];
  TextStyle _elementStyle =
      TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w500);
  TextStyle _buttonTextStyle =
      TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500);
  TextStyle _orderStyle =
      TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w400);
  @override
  void initState() {
    Api().getListOrder(widget.accessToken).then((list) {
      setState(() {
        _listOrder = list;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Mes annonces',
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: _listOrder.length == 0 ? _noneOrder() : _showListOrder(),
    );
  }
}
