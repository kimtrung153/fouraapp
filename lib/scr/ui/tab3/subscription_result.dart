import 'package:flutter/material.dart';

//SHOW ALL SUBSCRIPTION FOR USER CHOOSE
class SubscriptionResult extends StatefulWidget {
  @override
  _SubscriptionResultState createState() => _SubscriptionResultState();
}

class _SubscriptionResultState extends State<SubscriptionResult> {
  void _showDialog1() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Abonnement 12 mois à Foura',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            content: new RichText(
              text: TextSpan(
                  text: '''30,00 € HT / année avec 3 mois d’essai gratuit\n''',
                  style: TextStyle(
                    color: Colors.green,
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '''L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.

Certaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.

L’intérêt de s’abonner à Stock2Big est donc évident : pour 30 € HT par an, vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.''',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12,
                            fontWeight: FontWeight.w400)),
                  ]),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
              ),
            ],
          );
        });
  }

  void _showDialog2() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Center(
              child: Text(
                '''Abonnement 3 mois à''',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            content: new RichText(
              text: TextSpan(
                  text:
                      '''0,00 € HT / mois pour 3 mois avec 3 mois d’essai gratuit\n''',
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                  children: <TextSpan>[
                    TextSpan(
                      text:
                          '''L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.

Certaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.

Testez gratuitement pendant 3 mois notre service : vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.''',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                          fontWeight: FontWeight.w400),
                    ),
                  ]),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Boutique',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.only(top: 21, left: 16),
            child: Text(
              'Voici les 2 résultats',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
            height: 130,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.green,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  height: 108,
                  width: 193,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Abonnement 12 mois',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(
                          '''30,00 € HT / année avec 3 mois d’essai gratuit''',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          margin: const EdgeInsets.only(top: 6, bottom: 12),
                          height: 35,
                          width: 130,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                          child: Text(
                            'S\'abonner',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 108,
                  width: MediaQuery.of(context).size.width - 275,
                  margin: EdgeInsets.only(left: 10, right: 11),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          _showDialog1();
                        },
                        child: Container(
                          height: 22,
                          width: 22,
                          child: Image.asset(
                            'images/warningicon2.png',
                          ),
                        ),
                      ),
                      Container(
                        height: 66,
                        width: 104,
                        child: Image.asset(
                          'images/boutique.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
            height: 130,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.blue,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  height: 108,
                  width: 193,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Abonnement 3 mois',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(
                          '''0,00 € HT / mois pour 3 mois avec 3 mois d’essai gratuit''',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          margin: const EdgeInsets.only(top: 6, bottom: 12),
                          height: 35,
                          width: 130,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                          child: Text(
                            'S\'abonner',
                            style: TextStyle(
                                color: Colors.blue,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 108,
                  width: MediaQuery.of(context).size.width - 275,
                  margin: EdgeInsets.only(left: 10, right: 11),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          _showDialog2();
                        },
                        child: Container(
                          height: 22,
                          width: 22,
                          child: Image.asset(
                            'images/warningicon2.png',
                          ),
                        ),
                      ),
                      Container(
                        height: 66,
                        width: 104,
                        child: Image.asset(
                          'images/boutique.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
