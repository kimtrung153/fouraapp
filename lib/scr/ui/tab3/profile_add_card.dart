import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//ADD CARD IN PROFILE (CURRENT IS NONE)

class NonAddCardInProfile extends StatefulWidget {
  @override
  _NonAddCardInProfileState createState() => _NonAddCardInProfileState();
}

class _NonAddCardInProfileState extends State<NonAddCardInProfile> {
  void _showDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            backgroundColor: Colors.white,
            title: Text(
              'Sélectionnez la carte bancaire',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            content: Container(
              height: 175,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                          top: BorderSide(color: Colors.grey, width: 0.8),
                          bottom: BorderSide(color: Colors.grey, width: 0.8)),
                    ),
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 28,
                          width: 48,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.grey, width: 1)),
                          alignment: Alignment.center,
                          child: Image.asset(
                            'images/visa_1.png',
                            height: 9.1,
                            width: 30,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            '*1220',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 28,
                          width: 48,
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Image.asset(
                                'images/Group.png',
                                height: 14.7,
                                width: 24.43,
                              ),
                              Text(
                                'mastercard',
                                style: TextStyle(
                                    fontSize: 6,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            '*3242',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Cancel'),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Validation de la commande',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 21),
              child: Text(
                'Détails de facturation',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
            _inputSection('Prénom', context),
            _inputSection('Nom', context),
            _inputSection('Nom de l’entreprise', context),
            _inputSection('SIREN', context),
            _inputSection('RPPS', context),
            Container(
              height: 50,
              margin: const EdgeInsets.only(top: 20, left: 40, right: 40),
              alignment: Alignment.topCenter,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(left: 15),
                    width: 50,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Pays',
                      style: TextStyle(color: Colors.grey, fontSize: 14),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width - 240),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'France',
                      style: TextStyle(color: Colors.black, fontSize: 14),
                    ),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(Icons.keyboard_arrow_down),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
            _inputSection('Numéro et nom de rue', context),
            _inputSection('Code postal (facultatif)', context),
            _inputSection('Ville', context),
            _inputSection('Téléphone', context),
            _inputSection('Adresse de messagerie', context),
            Padding(
              padding: const EdgeInsets.only(left: 20, top: 42),
              child: Text(
                'Votre commande',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: _information(
                  'Produit', 'Total', Colors.black, FontWeight.bold, 14),
            ),
            Container(
              margin: const EdgeInsets.only(top: 17),
              padding: const EdgeInsets.only(
                top: 13,
              ),
              height: 56,
              decoration: BoxDecoration(
                  border: Border(
                bottom: BorderSide(color: Colors.grey, width: 0.8),
                top: BorderSide(color: Colors.grey, width: 0.8),
              )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _information('Abonnement 3', '0,00 € / mois pour 3',
                      Colors.grey, FontWeight.w400, 12),
                  _information(
                      'mois à Foura × 1',
                      'mois avec 3 mois d’essai gratuit',
                      Colors.grey,
                      FontWeight.w400,
                      12)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 13),
              child: _information(
                  'Sous-total', '0,00 €', Colors.black, FontWeight.w600, 14),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 25, bottom: 11),
              child: _information(
                  'Total', '0,00 €', Colors.black, FontWeight.w600, 14),
            ),
            Container(
              height: 40,
              padding: const EdgeInsets.only(left: 22),
              decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.grey, width: 0.8),
                    top: BorderSide(color: Colors.grey, width: 0.8)),
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                'Totaux Récurrents',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
              ),
            ),
            Container(
              height: 40,
              alignment: Alignment.centerLeft,
              child: _information('Sous-total', '0,00 € / mois pour 3 mois',
                  Colors.black, FontWeight.w600, 14),
            ),
            Container(
              height: 75,
              padding: const EdgeInsets.only(top: 15),
              decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 0.8,
                    ),
                    top: BorderSide(color: Colors.grey, width: 0.8)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  _information('Total récurrent', '0,00 € / mois pour 3 mois',
                      Colors.black, FontWeight.w600, 14),
                  Padding(
                    padding: const EdgeInsets.only(right: 16, top: 10),
                    child: Text(
                      '1er renouvellement : 06/08/2019',
                      style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.w400,
                          fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12, left: 22),
              child: Text(
                'Carte bancaire',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w600),
              ),
            ),
            GestureDetector(
              onTap: () {
                _showDialog();
              },
              child: Container(
                margin: const EdgeInsets.fromLTRB(40, 17, 40, 0),
                padding: const EdgeInsets.only(
                  left: 16,
                  right: 15,
                ),
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(5),
                ),
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Carte bancaire',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                height: 22,
                margin: const EdgeInsets.only(right: 40, top: 10),
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image.asset(
                      'images/addicon.png',
                      height: 22,
                      width: 22,
                      fit: BoxFit.cover,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        'Ajouter une nouvelle carte',
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                margin: const EdgeInsets.fromLTRB(43, 17, 37, 62),
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.blue,
                ),
                alignment: Alignment.center,
                child: Text(
                  'S\'abonner',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Container _inputSection(String txt, BuildContext context) => Container(
      margin: const EdgeInsets.only(left: 40, right: 40, top: 20),
      height: 50,
      width: MediaQuery.of(context).size.width - 80,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.grey[200],
      ),
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 15.5),
        child: Text(
          txt,
          style: TextStyle(
              color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w400),
        ),
      ),
    );

Row _information(
        String txt1, String txt2, Color color, FontWeight fontw, double fs) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 22),
          child: Text(
            txt1,
            style: TextStyle(color: color, fontWeight: fontw, fontSize: fs),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 16),
          child: Text(
            txt2,
            style: TextStyle(color: color, fontWeight: fontw, fontSize: fs),
          ),
        ),
      ],
    );
