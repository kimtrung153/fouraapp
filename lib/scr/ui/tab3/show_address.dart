import 'package:flutter/material.dart';
import 'package:foura_app/scr/models/user_infomation.dart';
import 'package:foura_app/scr/ui/tab3/fill_new_address.dart';

// USER CHANGE HIS/HER ADDRESS HERE

class ShowAddressScreen extends StatefulWidget {
  String accessToken;
  ShowAddressScreen({this.accessToken});
  @override
  _ShowAddressScreenState createState() => _ShowAddressScreenState();
}

class _ShowAddressScreenState extends State<ShowAddressScreen> {
  @override
  // void initState() {
  //   PostUserInfo().postUserInfo(widget.accessToken).then((data) {
  //     setState(() {
  //       userInfo = data;
  //     });
  //   });
  //   super.initState();
  // }
  UserInfo userInfo;
  TextStyle _textStyle = TextStyle(
      color: Colors.black87, fontSize: 14, fontWeight: FontWeight.w400);
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            "Modifier mon adresse",
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: userInfo == null
          ? Center(child: CircularProgressIndicator())
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.fromLTRB(16, 9, 12, 9),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.8))),
                  child: Text(
                    '''Les adresses suivantes seront utilisées par défaut sur la page de commande.''',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                userInfo.company == null
                    ? GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => AddNewAddressScreen(
                                        accessToken: widget.accessToken,
                                      )));
                        },
                        child: Container(
                          margin: const EdgeInsets.fromLTRB(20, 10, 15, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Image.asset(
                                    "images/addicon.png",
                                    height: 22,
                                    width: 22,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    'Adresse de facturation',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                size: 25,
                                color: Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      )
                    : GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => AddNewAddressScreen(
                                        accessToken: widget.accessToken,
                                        userInfo: userInfo,
                                      ))).then((onValue) {
                            if(onValue!= null){
                              setState(() {
                              userInfo = onValue;
                            });
                            }
                          });
                        },
                        child: Container(
                          height: 90,
                          margin: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black54, width: 1),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    height: 90,
                                    alignment: Alignment.topLeft,
                                    child: Image.asset(
                                      "images/locationicon.png",
                                      height: 16,
                                      width: 16,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Text(
                                        userInfo.company.toUpperCase(),
                                        style: _textStyle,
                                      ),
                                      Text(
                                        "${userInfo.lastName} ${userInfo.firstName}",
                                        style: _textStyle,
                                      ),
                                      Text(
                                        userInfo.street,
                                        style: _textStyle,
                                      ),
                                      Text(
                                        "${userInfo.postalCode} ${userInfo.city}",
                                        style: _textStyle,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                size: 25,
                                color: Colors.black45,
                              )
                            ],
                          ),
                        ),
                      ),
              ],
            ),
    );
  }
}
