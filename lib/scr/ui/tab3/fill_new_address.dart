import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'dart:convert';
import 'package:foura_app/scr/models/user_infomation.dart';

// import 'package:http/http.dart' as http;

//USER ADD NEW ADDRESS HERE

class AddNewAddressScreen extends StatefulWidget {
  UserInfo userInfo;
  String accessToken;
  AddNewAddressScreen({@required this.accessToken, this.userInfo});
  @override
  _AddNewAddressScreenState createState() => _AddNewAddressScreenState();
}

class _AddNewAddressScreenState extends State<AddNewAddressScreen> {
  TextEditingController _preNameController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _companyController = TextEditingController();
  TextEditingController _streetController = TextEditingController();
  TextEditingController _postalController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _countryContrller = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    _preNameController.dispose();
    _nameController.dispose();
    _companyController.dispose();
    _streetController.dispose();
    _postalController.dispose();
    _cityController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _countryContrller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    if (widget.userInfo != null) {
      _cityController.text = widget.userInfo.city;
      _companyController.text = widget.userInfo.company;
      _countryContrller.text = widget.userInfo.country;
      _emailController.text = widget.userInfo.email;
      _nameController.text = widget.userInfo.lastName;
      _phoneController.text = widget.userInfo.phoneNumber;
      _postalController.text = widget.userInfo.postalCode;
      _preNameController.text = widget.userInfo.firstName;
      _streetController.text = widget.userInfo.street;
    }
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          title: Text(
            'Modifier mon adresse',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 16, top: 21, right: 16),
            child: Text(
              '''Les adresses suivantes seront utilisées par défaut sur la page de commande.''',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _preNameController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Prénom",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _nameController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Nom",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _companyController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Nom de l’entreprise",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 40, right: 40, top: 20),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            alignment: Alignment.centerLeft,
            child: TextField(
              controller: _countryContrller,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Pays",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _streetController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Numéro et nom de rue",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _postalController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Code postal",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _cityController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Ville",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _phoneController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Téléphone",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            height: 50,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _emailController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Adresse de messagerie",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 80, 40, 40),
            child: RaisedButton(
              onPressed: () {
                Api()
                    .postChangeAddress(
                        widget.accessToken,
                        _preNameController.text,
                        _nameController.text,
                        _companyController.text,
                        _streetController.text,
                        _postalController.text,
                        _countryContrller.text,
                        _cityController.text,
                        _phoneController.text)
                    .then((addressResponse) {
                  if (addressResponse.statusCode == 200) {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content:
                          Text(json.decode(addressResponse.body)["message"]),
                    ));
                    UserInfo userInfo;
                    // PostUserInfo()
                    //     .postUserInfo(widget.accessToken)
                    //     .then((onValue) {
                    //   setState(() {
                    //     userInfo = onValue;
                    //   });
                    // });
                    // Timer(Duration(milliseconds: 1000), () {
                    //   Navigator.pop(context, userInfo);
                    // });
                  } else
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content:
                          Text(json.decode(addressResponse.body)["message"]),
                    ));
                });
              },
              color: Colors.blue,
              padding: const EdgeInsets.all(15),
              child: Text(
                'Enregistrer les modifications',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
