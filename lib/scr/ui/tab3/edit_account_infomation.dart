import 'package:flutter/material.dart';
import 'package:foura_app/scr/models/user_infomation.dart';

// DETAIL OF ACCOUNT
//USER CAN EDIT INFORMATION

class AccountDetailScreen extends StatefulWidget {
  String accessToken;
  UserInfo userInfo;
  AccountDetailScreen({@required this.accessToken,this.userInfo});
  @override
  _AccountDetailScreenState createState() => _AccountDetailScreenState();
}

class _AccountDetailScreenState extends State<AccountDetailScreen> {
  
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _shownNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _currentPassWordController = TextEditingController();
  TextEditingController _newPassWordController = TextEditingController();
  TextEditingController _confirmPassWordController = TextEditingController();
  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _shownNameController.dispose();
    _emailController.dispose();
    _currentPassWordController.dispose();
    _newPassWordController.dispose();
    _confirmPassWordController.dispose();
    super.dispose();
  }
  @override
  void initState() {
    if(widget.userInfo!=null){
      _firstNameController.text = widget.userInfo.firstName;
      _lastNameController.text = widget.userInfo.lastName;
      _emailController.text = widget.userInfo.email;
      _shownNameController.text = widget.userInfo.userName;
    }
    super.initState();
  }
  @override  
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Détails du compte',
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 50,
            margin: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _firstNameController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Prénom",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _lastNameController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Nom",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _shownNameController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Nom affiché",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
            child: Text(
              "Indique comment votre nom apparaîtra dans la section relative au compte et dans les avis",
              style: TextStyle(color: Colors.grey,fontSize: 15,fontWeight: FontWeight.w400),
              textAlign: TextAlign.justify,
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _emailController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Email",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
            child: Text(
              "Changement de mot de passe",
              style: TextStyle(color: Colors.grey,fontSize: 15,fontWeight: FontWeight.w400),
              textAlign: TextAlign.justify,
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _currentPassWordController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Mot de passe actuel",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _newPassWordController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Nouveau mot de passe",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _confirmPassWordController,
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              decoration: InputDecoration(
                hintText: "Confirmer le nouveau mot de passe",
                hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                contentPadding: const EdgeInsets.only(left: 15),
                border: InputBorder.none,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(40),
            child: RaisedButton(
              onPressed: (){},
              color: Colors.blue,
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                "Enregistrer les modifications",
                style: TextStyle(
                  color: Colors.white,fontSize: 16,fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
