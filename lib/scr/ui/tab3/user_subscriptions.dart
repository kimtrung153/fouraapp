import 'package:flutter/material.dart';

//USER'S SUBSCRIPTIONS 

class UserSubscriptionScreen extends StatefulWidget {
  @override
  _UserSubscriptionScreenState createState() => _UserSubscriptionScreenState();
}

class _UserSubscriptionScreenState extends State<UserSubscriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          elevation: 5,
          title: Text(
            'Mon abonnement',
            style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20,left: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Vous n\'avez pas d\'abonnements actifs. Trouvez votre premier abonnement dans la boutique',
              style: TextStyle(color: Colors.grey,fontSize: 14,fontWeight: FontWeight.w400),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height -258,
                left: 40,
                right: 40,
              ),
              child: GestureDetector(
                onTap: (){},
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width-80,
                  alignment: Alignment.center,
                  child: Text(
                    'Boutique',
                    style: TextStyle(color: Colors.white,fontSize: 16,fontWeight: FontWeight.w500),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      
    );
  }
}