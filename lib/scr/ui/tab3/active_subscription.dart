import 'package:flutter/material.dart';

// REGISTER ACTIVE SUBSCRIPTIONS

class ActiveSubscription extends StatefulWidget {
  @override
  _ActiveSubscriptionState createState() => _ActiveSubscriptionState();
}

class _ActiveSubscriptionState extends State<ActiveSubscription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Mes annonces',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 50,
            margin: const EdgeInsets.only(top: 20, left: 16),
            child: Text(
              'Vous n\'avez pas d\'abonnements actifs. Trouvez votre premier abonnement dans la boutique.',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
          GestureDetector(
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width - 80,
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height - 280,
                  left: 40,
                  right: 40),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(5.0)),
              child: Text(
                'Boutique',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
