import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'package:stripe_api/stripe_api.dart';
import 'package:credit_card_field/credit_card_field.dart';
import 'dart:convert';

import 'add_bank_screen.dart';
//ADD A PAYMENT METHOD

class AddPaymentMethod extends StatefulWidget {
  String accesstoken;
  AddPaymentMethod({this.accesstoken});
  @override
  _AddPaymentMethodState createState() => _AddPaymentMethodState();
}

class _AddPaymentMethodState extends State<AddPaymentMethod> {
  final _userNameController = TextEditingController();
  final _cardNumberController = TextEditingController();
  final _expiryDatecontroller = TextEditingController();
  final _cvcController = TextEditingController();
  final _hintStyle = TextStyle(color: Colors.grey,fontSize: 14,fontWeight: FontWeight.w500);
  final _textstyle = TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.w400);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    Stripe.init('pk_test_u6D6abcpm6v1anhGIFFNQ87j00RdMbJ8vj');
    super.initState();
  }
  @override
  void dispose(){
    _userNameController.dispose();
    _cardNumberController.dispose();
    _expiryDatecontroller.dispose();
    _cvcController.dispose();
    super.dispose();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Moyens de paiement',
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(left: 16, top: 10, right: 12),
            height: 96,
            width: MediaQuery.of(context).size.width - 28,
            child: RichText(
              text: TextSpan(
                text:
                    '''Payez avec votre carte bancaire en toute sécurité. MODE TEST ACTIVÉ. En mode test, vous pouvez utiliser le numéro de carte 4242424242424242 avec n’importe quel cryptogramme visuel et une date d’expiration valide ou consulter la ''',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 13,
                    fontWeight: FontWeight.w400),
                children: <TextSpan>[
                  TextSpan(
                    text: '''documentation Test Stripe ''',
                    style: TextStyle(
                        color: Colors.green,
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ),
                  TextSpan(
                    text: '''pour obtenir plus de numéros de carte.''',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, bottom: 16),
            width: MediaQuery.of(context).size.width,
            height: 43,
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 0.8))),
            alignment: Alignment.topCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 27,
                  width: 44,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Image.asset(
                    'images/visa.png',
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 27,
                  width: 44,
                  margin: const EdgeInsets.only(left: 3.8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Image.asset(
                    'images/mastercard.png',
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 27,
                  width: 44,
                  margin: const EdgeInsets.only(left: 3.8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Image.asset(
                    'images/amex.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 43),
            child: Text(
              'Name on card',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 43, right: 37, top: 12),
            height: 50,
            width: MediaQuery.of(context).size.width - 80,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 15.5),
              child: TextField(
                controller: _userNameController,
                cursorColor: Colors.black45,
                style: _textstyle,
                decoration: InputDecoration(
                  hintText: 'Name on card',
                  hintStyle: _hintStyle,
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25, left: 43),
            child: Text(
              'Card number',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 43, right: 37, top: 12),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5)),
            child: TextField(
              controller: _cardNumberController,
              cursorColor: Colors.black38,
              maxLength: 16,
              decoration: InputDecoration(
                hintText: "Card Number",
                hintStyle: _hintStyle,
                border: InputBorder.none,
                contentPadding: const EdgeInsets.only(top: 40,),
                prefixIcon: Container(
                  decoration: BoxDecoration(
                    border: Border(right: BorderSide(color: Colors.grey,width: 0.8)),
                  ),
                  margin: const EdgeInsets.only(top: 20,right: 5),
                  child: Image.asset(
                    "images/normalcard.png",
                  ),
                )
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 42, right: 37, top: 16),
            height: 88,
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 9),
                      child: Text(
                        'Expiry date',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 12),
                      height: 50,
                      width: 130,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.grey[200],
                      ),
                      alignment: Alignment.centerLeft,
                      child: ExpirationFormField(
                        controller: _expiryDatecontroller,
                        obscureText: false,
                        enabled: true,
                        decoration: InputDecoration(
                          hintText: "MM/YY",
                          hintStyle: _hintStyle,
                          border: InputBorder.none,
                          contentPadding: const EdgeInsets.only(left: 8),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 9),
                        child: Text(
                          'Security code',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 12),
                        height: 50,
                        width: 130,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.grey[200],
                        ),
                        alignment: Alignment.centerRight,
                        child: TextField(
                          controller: _cvcController,
                          cursorColor: Colors.grey,
                          decoration: InputDecoration(
                            hintText: "CVC",
                            hintStyle: _hintStyle,
                            border: InputBorder.none,
                            contentPadding: const EdgeInsets.only(left: 8)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () async {
              StripeCard card = StripeCard(
                number: _cardNumberController.text,
                expMonth: int.parse(_expiryDatecontroller.text.toString().substring(0,2)),
                expYear: int.parse(_expiryDatecontroller.text.toString().substring(3,5)),
                cvc: _cvcController.text,
              );
                Stripe.instance.createCardToken(card).then((token) async {
                await Api().postAddCard(widget.accesstoken, token.id).then((response){
                 if(json.decode(response.body)["success"]==true){
                   Navigator.push(context, MaterialPageRoute(builder: (_)=>AddBankScreen(accessToken: widget.accesstoken)));
                 } else
                _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(json.decode(response.body)["message"]),));
                });
              });
            },
            child: Container(
              margin:
                  EdgeInsets.only(top: 100, bottom: 53, left: 43, right: 37),
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
              child: Text(
                'Ajouter un moyen de paiement',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
