import 'package:flutter/material.dart';
import 'package:foura_app/scr/ui/tab1/registation_screen.dart';

//DRUGS ARE NOT FOUND AFTER SEARCH 

class NoResultAfterSearch extends StatefulWidget {
  @override
  _NoResultAfterSearchState createState() => _NoResultAfterSearchState();
}

class _NoResultAfterSearchState extends State<NoResultAfterSearch> {
  TextStyle _textStyle = TextStyle(color: Colors.grey, fontSize: 13, fontWeight: FontWeight.w300);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Recherche',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontSize: 20,
            ),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 20,left: 16,right: 18),
            child: Text(
              '''Aucun résultat pour votre recherche. Soyez le premier à soumettre une annonce pour ce département.''',
              style: _textStyle,
            ),
          ),
          //SUGGEST USER TO CREATE AN ADVERTISEMENT FOR DRUG
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SubmitADScreen()));
            },
            child: Container(
              height: 50,
              margin: EdgeInsets.only(top: 400,left: 40,right: 40),
              width: MediaQuery.of(context).size.width - 80,
              decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(5.0)),
              alignment: Alignment.center,
              child: Text(
                'Soumettre une annonce',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
