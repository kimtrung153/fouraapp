import 'package:flutter/material.dart';
import 'package:foura_app/scr/ui/tab1/search_no_result.dart';

// USERS SEARCH DRUGS THEY WANT TO FIND HERE

class SearchDrugScreen extends StatefulWidget {
  @override
  _SearchDrugScreenState createState() => _SearchDrugScreenState();
}

class _SearchDrugScreenState extends State<SearchDrugScreen> {
  TextStyle _textStyle =
      TextStyle(color: Colors.grey, fontSize: 15, fontWeight: FontWeight.w500);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: Colors.blue,
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Recherche',
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 16, left: 40, right: 40),
        children: <Widget>[
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.white,
            ),
            child: TextField(
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Nom du produit, code ACL',
                hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                  size: 21,
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 16),
            child: GestureDetector(
              onTap: () {},
              child: Container(
                height: 50,
                padding: const EdgeInsets.symmetric(horizontal: 18),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Tous les départements',
                      style: _textStyle,
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      size: 25,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Container(
              height: 50,
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 16),
              padding: const EdgeInsets.symmetric(horizontal: 18),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Trier par',
                    style: _textStyle,
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 25,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NoResultAfterSearch()));
            },
            child: Container(
              height: 50,
              margin: const EdgeInsets.only(
                top: 250,
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(5.0)),
              child: Text(
                'Rechercher',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
