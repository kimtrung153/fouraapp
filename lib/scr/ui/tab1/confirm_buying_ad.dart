import 'package:flutter/material.dart';

import 'buying_advertisement_of_user.dart';

//CONFIRM BUYING ADVERTISEMENT AFTER ADDING INFORMATION 
class ConfirmAD extends StatefulWidget {
  @override
  _ConfirmADState createState() => _ConfirmADState();
}

class _ConfirmADState extends State<ConfirmAD> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Commande reçue',
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _text('Merci. Votre commande a été reçue.', Colors.grey,FontWeight.w400, 16, 20),
                _text('NUMÉRO DE COMMANDE :', Colors.grey, FontWeight.w400, 16,22),
                _text('GRATUIT-23', Colors.black, FontWeight.w400, 14, 9),
                _text('DATE :', Colors.grey, FontWeight.w400, 16, 23),
                _text('30/05/2019', Colors.black, FontWeight.w400, 14, 9),
                _text('E-MAIL :', Colors.grey, FontWeight.w400, 16, 23),
                _text('joannejemison121@gmail.com', Colors.black,FontWeight.w400, 14, 9),
                _text('TOTAL :', Colors.grey, FontWeight.w400, 16, 23),
                _text('0,00 €', Colors.black, FontWeight.w400, 14, 9),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 21, bottom: 13),
            padding: const EdgeInsets.only(left: 20),
            height: 96,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(color: Colors.grey, width: 0.8),
                  bottom: BorderSide(color: Colors.grey, width: 0.8)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _text(
                    'Votre abonnement sera actif quand votre règlement sera effectué',
                    Colors.grey,
                    FontWeight.w400,
                    14,
                    18),
                _text('Voir le statut de votre abonnement dans votre compte',
                    Colors.grey, FontWeight.w400, 14, 10),
              ],
            ),
          ),
          _row('Produit', 'Total', Colors.black, FontWeight.bold, 16),
          Container(
            height: 56,
            margin: const EdgeInsets.only(top: 17),
            padding: const EdgeInsets.only(top: 13, bottom: 10),
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(color: Colors.grey, width: 0.8),
                  top: BorderSide(color: Colors.grey, width: 0.8)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Abonnement 3',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                      RichText(
                        text: TextSpan(
                            text: 'mois à Foura ',
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                                fontWeight: FontWeight.w400),
                            children: <TextSpan>[
                              TextSpan(
                                text: 'x1',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: Text(
                    '0,00 €',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 13),
            child:
                _row('Sous-total', '0,00 €', Colors.black, FontWeight.w600, 14),
          ),
          Container(
            height: 41,
            margin: const EdgeInsets.only(top: 11),
            padding: const EdgeInsets.only(top: 12, bottom: 11),
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(color: Colors.grey, width: 0.8),
                  top: BorderSide(color: Colors.grey, width: 0.8)),
            ),
            child: _row('Total', '0,00 €', Colors.black, FontWeight.w600, 14),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>BuyingAdvertisementOfUser()));
            },
            child: Container(
              height: 50,
              margin: const EdgeInsets.fromLTRB(40, 48, 40, 35),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.blue,
              ),
              alignment: Alignment.center,
              child: Text(
                'Votre compte',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Padding _text(String txt, Color color, FontWeight fontw, double fonts, double pad) =>Padding(
      padding: EdgeInsets.only(top: pad),
      child: Text(
        txt,
        style: TextStyle(
          color: color,
          fontSize: fonts,
          fontWeight: fontw,
        ),
      ),
    );

Row _row(String txt1, String txt2, Color color, FontWeight fontw,double fonts) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 23),
          child: Text(
            txt1,
            style: TextStyle(
              color: color,
              fontSize: fonts,
              fontWeight: fontw,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: Text(
            txt2,
            style: TextStyle(
              color: color,
              fontSize: fonts,
              fontWeight: fontw,
            ),
          ),
        ),
      ],
    );
