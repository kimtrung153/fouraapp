import 'package:flutter/material.dart';


//SHOW FINDING RESULT (FINDING RESULT IS NOT NULL)
class ResultScreen extends StatefulWidget {
  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('Recherche',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.w500),),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height-60,
        width: MediaQuery.of(context).size.width,
        color: Colors.grey[200],
        alignment: Alignment.topCenter,
        padding: EdgeInsets.only(top: 20),
        child: GestureDetector(
          onTap: (){},
          child: Container(
            margin: const EdgeInsets.only(left: 40,right: 40),
            height: 80,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.white,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20,top: 12),
                  width: 80,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Novalgin',
                        style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 15),
                        ),
                      Container(
                        margin: const EdgeInsets.only(top: 5,bottom: 5),
                        child: Text(
                          '3413257',
                          style: TextStyle(color: Colors.grey,fontSize: 13,fontWeight: FontWeight.w500)
                        ),
                      ),
                      Text(
                        'Cher',
                        style: TextStyle(color: Colors.green,fontWeight: FontWeight.w500,fontSize: 13),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width-300,top: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        '24/04/2019',
                        style: TextStyle(color: Colors.grey,fontSize: 15,fontWeight: FontWeight.w500),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5),
                        child: Text(
                          '3.5€',
                          style: TextStyle(color: Colors.green,fontWeight: FontWeight.w500,fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  child: Icon(
                    Icons.keyboard_arrow_right,
                    color: Colors.grey,
                    size: 29,
                    ),
                ),
              ],
            ),
          ),
        ),
      ),
      
    );
  }
}