import 'package:flutter/material.dart';

import 'confirmation_order.dart';


// SUGGEST USER TO CREATE AN BUYING ADVERTISEMENT WITH 3 MONTHS FREE TRIAL 

class SubmitADScreen extends StatefulWidget {
  @override
  _SubmitADScreenState createState() => _SubmitADScreenState();
}

class _SubmitADScreenState extends State<SubmitADScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'S’abonner',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.only(left: 20,top: 20),
        child: ListView(
          children: <Widget>[
            Text(
              'Abonnement 3 mois à',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 22),
              child: RichText(
                text: TextSpan(
                  text:
                      '0,00 € HT / mois pour 3 mois avec 3 mois d’essai gratuit\n\n',
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 14,
                      fontWeight: FontWeight.w300),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '''L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.\n\nCertaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.\n\nTestez gratuitement pendant 3 mois notre service : vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.''',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w300)),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height - 456,
                  left: 20,
                  right: 20),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>ConfirmationOrder()));
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    'S\'abonner',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
