import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'confirm_buying_ad.dart';

//ADD INFORMATION OF NEW BUYING ADVERTISEMENT

class ConfirmationOrder extends StatefulWidget {
  @override
  _ConfirmationOrderState createState() => _ConfirmationOrderState();
}

class _ConfirmationOrderState extends State<ConfirmationOrder> {
  TextStyle _infoStyle =
      TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold);
  TextStyle _detailStyle =
      TextStyle(color: Colors.grey, fontWeight: FontWeight.w500, fontSize: 12);
  TextStyle _hintStyle =
      TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w400);

  TextEditingController _firstNamecontroller = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _companyController =TextEditingController();
  TextEditingController _sirenController = TextEditingController();
  TextEditingController _rppsController = TextEditingController();
  TextEditingController _countryController = TextEditingController();
  TextEditingController _streetController = TextEditingController();
  TextEditingController _postalCodeController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _emailController = TextEditingController();

  @override
  void dispose() {
    _firstNamecontroller.dispose();
    _nameController.dispose();
    _companyController.dispose();
    _sirenController.dispose();
    _rppsController.dispose();
    _countryController.dispose();
    _streetController.dispose();
    _postalCodeController.dispose();
    _cityController.dispose();
    _phoneNumberController.dispose();
    _emailController.dispose();
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: Colors.white,
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Validation de la commande',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: <Widget>[
          Text(
            'Détails de facturation',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _firstNamecontroller,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Prénom",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _nameController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Nom",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _companyController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Nom de l’entreprise",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _sirenController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "SIREN",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _rppsController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "RPPS",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _countryController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Pays",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _streetController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Numéro et nom de rue",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _postalCodeController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Code postal (facultatif)",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _cityController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Ville",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _phoneNumberController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Téléphone'",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            child: TextField(
              controller: _emailController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Adresse de messagerie",
                hintStyle: _hintStyle,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Votre commande',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16),
          ),
          Container(
            height: 50,
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 0.8))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Produit',
                  style: _infoStyle,
                ),
                Text(
                  'Total',
                  style: _infoStyle,
                ),
              ],
            ),
          ),
          Container(
            height: 60,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 0.8))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Abonnement 3",
                      style: _detailStyle,
                    ),
                    Text(
                      "mois à Foura × 1",
                      style: _detailStyle,
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "0,00 € / mois pour 3",
                      style: _detailStyle,
                    ),
                    Text(
                      "mois avec 3 mois d’essai gratuit",
                      style: _detailStyle,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 90,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 0.8))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Sous-tota",
                      style: _infoStyle,
                    ),
                    Text(
                      "Total",
                      style: _infoStyle,
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "0,00 €",
                      style: _infoStyle,
                    ),
                    Text(
                      "0,00 €",
                      style: _infoStyle,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 40,
            padding: const EdgeInsets.only(left: 20),
            decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(color: Colors.grey, width: 0.8)),
            ),
            alignment: Alignment.centerLeft,
            child: Text(
              'Totaux Récurrents',
              style: _infoStyle,
            ),
          ),
          Container(
            height: 40,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(color: Colors.grey, width: 0.8)),
            ),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Sous-total',
                  style: _infoStyle,
                ),
                Text(
                  '0,00 € / mois pour 3 mois',
                  style: _infoStyle,
                ),
              ],
            ),
          ),
          Container(
            height: 40,
            margin: const EdgeInsets.symmetric(horizontal: 20),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Total récurrent',
                  style: _infoStyle,
                ),
                Text(
                  '0,00 € / mois pour 3 mois',
                  style: _infoStyle,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 20),
            alignment: Alignment.centerRight,
            child: Text(
              '1er renouvellement : 06/08/2019',
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.w500,
                  fontSize: 12),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 50, 20, 20),
            child: RaisedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ConfirmAD()));
              },
              color: Colors.blue,
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                "S\'abonner",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
