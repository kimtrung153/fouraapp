import 'package:flutter/material.dart';
import 'package:foura_app/scr/ui/home_screens/home_screen.dart';
//SHOW ALL BUYING ADVERTISEMENT OF USER

class BuyingAdvertisementOfUser extends StatefulWidget {
  @override
  _BuyingAdvertisementOfUserState createState() => _BuyingAdvertisementOfUserState();
}

class _BuyingAdvertisementOfUserState extends State<BuyingAdvertisementOfUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(
            'Mon abonnement',
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back,color: Colors.black,),
            onPressed: (){
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_)=>MainScreen()), ModalRoute.withName('/'));
            },
          ),
        ),
      ),
      body: Container(
        height: 131,
        margin: const EdgeInsets.fromLTRB(20, 21, 20, 0),
        padding: const EdgeInsets.only(left: 17),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _rowText('ID: ', '#1009'),
                _rowText('État: ', 'Actif'),
                _rowText('Prochain paiement: ', '30/08/2019'),
                _rowText('Total: ', '0,00 €'),
              ],
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.arrow_forward_ios,
                color: Colors.grey,
                size: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Row _rowText(String txt1, String txt2) => Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          txt1,
          style: TextStyle(
              color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w500),
        ),
        Text(
          txt2,
          style: TextStyle(
            color: Colors.black,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );