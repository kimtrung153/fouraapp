import 'package:flutter/material.dart';
import 'package:foura_app/scr/models/drug_infomation_model.dart';
import 'package:foura_app/scr/ui/tab1/detail_about_drug.dart';
import 'package:foura_app/scr/ui/tab1/search_drug_screen.dart';

//default screen of tab 1
//THIS SCREEN SHOW THE LIST OF DRUGS CAN BE SOLD

class MedicalScreen extends StatefulWidget {
  final String accessToken;
  List<DrugInfo> listDrug;

  MedicalScreen({@required this.accessToken,this.listDrug});
  @override
  _MedicalScreenState createState() => _MedicalScreenState();
}

class _MedicalScreenState extends State<MedicalScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(87),
        child: Container(
          padding: const EdgeInsets.only(
              top: 32.0, left: 20.0, right: 20.0, bottom: 10.0),
          height: 87.0,
          width: MediaQuery.of(context).size.width,
          color: Colors.blue,
          child: GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchDrugScreen()));
            },
            child: Container(
              height: 45.0,
              padding: const EdgeInsets.only(left: 16.3),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.white,
              ),
              alignment: Alignment.centerLeft,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.grey,
                    size: 22,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Text(
                      'Nom du produit, code ACL',
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: ListView.builder(
              padding: const EdgeInsets.only(bottom: 20),            
              itemCount: widget.listDrug.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailAboutDrug(id: widget.listDrug[index].id,)));
                  },
                  child: Container(
                    height: 80.0,
                    margin: const EdgeInsets.only(
                      top: 10,
                      left: 20.0,
                      right: 20.0,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(left: 21.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(widget.listDrug[index].title,
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  )),
                              Text(
                                widget.listDrug[index].alcCode,
                                style: TextStyle(
                                    color: Colors.grey[400],
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                widget.listDrug[index].workPlaceName,
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  DateTime.fromMillisecondsSinceEpoch(widget.listDrug[index].exprireDate*1000).toString().substring(0,10),
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  '${widget.listDrug[index].price}€',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                            Icon(
                              Icons.chevron_right,
                              color: Colors.grey,
                              size: 30.0,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
    );
  }
}
