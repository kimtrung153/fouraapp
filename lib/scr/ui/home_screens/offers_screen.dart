import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'dart:convert';
import 'package:foura_app/scr/models/workplace.dart';
import 'package:foura_app/scr/ui/tab2/manual_instructions.dart';
import 'package:foura_app/scr/ui/tab3/all_advertisement.dart';

//default screen of tab 2 if user had subscribed service
//USER POST INFORMATION OF DRUGS THEY WANT TO PASS HERE

class OffersScreen extends StatefulWidget {
  final String accessToken;
  OffersScreen({this.accessToken});
  @override
  _OffersScreenState createState() => _OffersScreenState();
}

class _OffersScreenState extends State<OffersScreen> {
  //style of each box's text
  TextStyle _textStyle = TextStyle(
    color: Colors.black38,
    fontSize: 15,
    fontWeight: FontWeight.w400,
  );
  //style of workplace options
  TextStyle _pickerStyle = TextStyle(
    color: Colors.black54,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController _titleController = TextEditingController();
  TextEditingController _alcCodeController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  @override
  void dispose() {
    _titleController.dispose();
    _alcCodeController.dispose();
    _amountController.dispose();
    _priceController.dispose();
    super.dispose();
  }

  //show a susgestion about create an advertisement
  void _showDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            "Ajouter une annonce",
            style: TextStyle(
                color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
          ),
          content: GestureDetector(
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => InstructionScreen()));
            },
            child: new RichText(
              text: TextSpan(
                text:
                    '3 conseils à respecter pour une annonce efficace :\n 1. Utilisez votre logiciel pour',
                style: TextStyle(color: Colors.grey, fontSize: 12),
                children: <TextSpan>[
                  TextSpan(
                    text: ' créer une liste de produits invendus ',
                    style: TextStyle(color: Colors.green, fontSize: 12),
                  ),
                  TextSpan(
                    text:
                        '''depuis une à deux années. Facile à faire, elle vous permettra de cibler les produits risquant de périmer;\n2. Ne déposez pas une annonce avec une date de péremption trop proche. Le produit aura peu de chance d’être échangé ;\n3. N’hésitez pas à parler du site à vos confrères après le dépôt d’une annonce. Cela augmentera l’efficacité de la plateforme ;''',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  String _selectedPlace = 'Département';
  int selectItem;

  void _showWorkPlaceOptions() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height / 3,
            child: Scaffold(
              appBar: PreferredSize(
                preferredSize: Size.fromHeight(30),
                child: AppBar(
                  leading: Row(
                    children: <Widget>[
                      SizedBox(width: 5,),
                      Icon(Icons.keyboard_arrow_down,color: Colors.blue,),
                      Icon(Icons.keyboard_arrow_up,color: Colors.blue,),
                    ],
                  ),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          _selectedPlace = _listWorkplace[selectItem].name;
                        });
                        Navigator.pop(context);
                      },
                      child: Text(
                        'DONE',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ],
                ),
              ),
              body: Container(
                child: CupertinoPicker(
                  looping: false,
                  children:
                      List<Widget>.generate(_listWorkplace.length, (int index) {
                    return Text(
                      _listWorkplace[index].name,
                      style: _pickerStyle,
                    );
                  }),
                  magnification: 1,
                  itemExtent: 30,
                  backgroundColor: Colors.white,
                  onSelectedItemChanged: (index) {
                    selectItem = index;
                  },
                ),
              ),
            ),
          );
        });
  }

  bool _isPicked = false;
  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2018),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate)
      setState(() {
        _isPicked = true;
        selectedDate = picked;
      });
  }

  void initState() {
    super.initState();
    Api().postListWorkplace().then((listWorkplace) {
      setState(() {
        _listWorkplace = listWorkplace;
      });
    });
  }

  List<Workplace> _listWorkplace = [];

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: Text(
            'Petites Annonces',
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 15),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Ajouter une annonce',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              GestureDetector(
                onTap: () {
                  _showDialog();
                },
                child: Container(
                  height: 22,
                  width: 22,
                  margin: const EdgeInsets.only(left: 15),
                  child: Image.asset(
                    'images/warningicon.png',
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            alignment: Alignment.centerLeft,
            child: TextField(
              controller: _titleController,
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 15),
                hintText: "Title",
                hintStyle: _textStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            alignment: Alignment.centerLeft,
            child: TextField(
              controller: _alcCodeController,
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 15),
                hintText: "Code ALC",
                hintStyle: _textStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            alignment: Alignment.centerLeft,
            child: TextField(
              controller: _amountController,
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 15),
                hintText: "Quantité",
                hintStyle: _textStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(5),
            ),
            alignment: Alignment.centerLeft,
            child: TextField(
              controller: _priceController,
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 15),
                hintText: "Prix TTC + honoraires",
                hintStyle: _textStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              _selectDate(context);
            },
            child: Container(
              height: 50,
              margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5),
              ),
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    _isPicked == false
                        ? "Date de péremption"
                        : "${selectedDate.toString().substring(0, 10)}",
                    style: _textStyle,
                  ),
                  Icon(Icons.keyboard_arrow_down),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () => _showWorkPlaceOptions(),
            child: Container(
              height: 50,
              margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5),
              ),
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    _selectedPlace,
                    style: _textStyle,
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 25,
                    color: Colors.black,
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(40),
            child: RaisedButton(
              onPressed: () {
                Api()
                    .postcreateAD(
                  widget.accessToken,
                  _titleController.text,
                  _alcCodeController.text,
                  _amountController.text,
                  _priceController.text,
                  (selectItem + 1).toString(),
                  (selectedDate.millisecondsSinceEpoch ~/ 1000).toString(),
                )
                    .then((response) {
                  if (json.decode(response.body)["success"] == true) {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(json.decode(response.body)["message"]),
                    ));
                    Timer(Duration(milliseconds: 1000), () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => AllAdvertisement()));
                    });
                  } else
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(json.decode(response.body)["message"]),
                    ));
                });
              },
              color: Colors.blue,
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                'Créer une nouvelle annonce',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
