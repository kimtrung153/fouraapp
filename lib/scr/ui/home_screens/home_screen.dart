import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'package:foura_app/scr/models/drug_infomation_model.dart';
import 'package:foura_app/scr/models/user_infomation.dart';
import 'package:foura_app/scr/ui/home_screens/medical_screen.dart';
import 'package:foura_app/scr/ui/home_screens/offers_screen.dart';
import 'package:foura_app/scr/ui/home_screens/profile_screen.dart';
import 'package:foura_app/scr/ui/tab2/suggest_create_ad.dart';

//This creen will be shown when sign in success


class MainScreen extends StatefulWidget {
  String accessToken;
  UserInfo userInfo;
  MainScreen({this.accessToken, this.userInfo});
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentSelected = 0;
  void currentSeclected(index) {
    setState(() {
      _currentSelected = index;
    });
  }

  List<Widget> _selectedPage() => [
        MedicalScreen(accessToken: widget.accessToken,listDrug: _listDrug,),
        widget.userInfo.subscription.length == 0
            ? SuggestUserPostAD()
            : OffersScreen(accessToken: widget.accessToken),
        ProfileScreen(
          accessToken: widget.accessToken,
          userInfo: widget.userInfo,
        ),
      ];

  List<DrugInfo> _listDrug = [];
  @override
  void initState() {
    super.initState();
    Api().getListDrug().then((listDrug){
      setState(() {
       _listDrug = listDrug; 
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (widget.userInfo == null || _listDrug.length == 0)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : _selectedPage()[_currentSelected],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentSelected,
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.blue[300],
        backgroundColor: Colors.white,
        elevation: 5,
        iconSize: 27.0,
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              _currentSelected == 0 ? 'images/icon12.png' : 'images/icon11.png',
              height: 30.0,
              width: 30.0,
            ),
            title: Text('Médicaments'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              _currentSelected == 1 ? 'images/icon22.png' : 'images/icon21.png',
              height: 30.0,
              width: 30.0,
            ),
            title: Text('Petites Annonces'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              _currentSelected == 2 ? 'images/icon32.png' : 'images/icon31.png',
              height: 30.0,
              width: 30.0,
            ),
            title: Text('Profile'),
          ),
        ],
        onTap: (index) {
          currentSeclected(index);
        },
      ),
    );
  }
}
