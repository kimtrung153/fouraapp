import 'package:flutter/material.dart';
import 'package:foura_app/scr/models/user_infomation.dart';
import 'package:foura_app/scr/ui/sign_screens/foura_screen.dart';
import 'package:foura_app/scr/ui/tab2/show_address.dart';
import 'package:foura_app/scr/ui/tab3/add_bank_screen.dart';
import 'package:foura_app/scr/ui/tab3/all_advertisement.dart';
import 'package:foura_app/scr/ui/tab3/all_order.dart';
import 'package:foura_app/scr/ui/tab3/edit_account_infomation.dart';
import 'package:foura_app/scr/ui/tab3/user_subscriptions.dart';

//default screen of tab 3
//THIS SCREEN SHOW INFORMATION OF USER


class ProfileScreen extends StatefulWidget {
  String accessToken;
  UserInfo userInfo;
  ProfileScreen({@required this.accessToken,this.userInfo});
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Container _container(String _imageName, String _pathName) {
    return Container(
      height: 50,
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 13),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                _imageName,
                height: 40,
                width: 40,
              ),
              SizedBox(
                width: 15,
              ),
              Text(
                _pathName,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
          Icon(
            Icons.chevron_right,
            size: 25,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: Text(
            'Profile',
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      body: widget.userInfo == null
          ? Center(child: CircularProgressIndicator())
          : ListView(
              padding: const EdgeInsets.only(bottom: 20),
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 15),
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: 90,
                    width: 90,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: ExactAssetImage(
                                  'images/avatar.jpg',
                                )),
                          ),
                        ),
                        //MISSING A CAMERA ICON
                      ],
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  margin: const EdgeInsets.only(top: 10, bottom: 5),
                  child: Text(
                    widget.userInfo.userName,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => AccountDetailScreen(
                                    accessToken: widget.accessToken,
                                    userInfo: widget.userInfo,
                                  )));
                    },
                    child: _container('images/1.png', 'Modifier mon compte')),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => AllAdvertisement()));
                  },
                  child: _container('images/2.png', 'Mes annonces'),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => UserSubscriptionScreen()));
                  },
                  child: _container('images/3.png', 'Mon abonnement'),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => OrdersOfUser(
                                  accessToken: widget.accessToken,
                                )));
                  },
                  child: _container('images/4.png', 'Mes commandes'),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => ShowAddressScreen(
                                  // accessToken: widget.accessToken,
                                )));
                  },
                  child: _container('images/5.png', 'Modifier mon adresse'),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => AddBankScreen(
                                  accessToken: widget.accessToken,
                                )));
                  },
                  child: _container('images/6.png', 'Méthodes de paiement'),
                ),
                GestureDetector(
                  onTap: () {},
                  child: _container('images/7.png', 'Mon abonnement'),
                ),
                SizedBox(
                  height: 50,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => FouraScreen()),
                        ModalRoute.withName('/'));
                  },
                  child: _container('images/8.png', 'Déconnexion'),
                ),
              ],
            ),
    );
  }
}
