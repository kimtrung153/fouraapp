import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura_app/scr/api/api.dart';
import 'package:foura_app/scr/api/authentication.dart';
import 'package:foura_app/scr/ui/sign_screens/foura_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          builder: (_) => Authentication(),
          dispose: (_, auth) => auth.dispose(),
        ),
        Provider(
          builder: (_)=> Api(),
          dispose: (_,api) => api.dispose(),
        ),
      ],
      child: MaterialApp(
        title: 'Foura App',
        // theme: ThemeData(
        //   // This is the theme of your application.
        //   //
        //   // Try running your application with "flutter run". You'll see the
        //   // application has a blue toolbar. Then, without quitting the app, try
        //   // changing the primarySwatch below to Colors.green and then invoke
        //   // "hot reload" (press "r" in the console where you ran "flutter run",
        //   // or simply save your changes to "hot reload" in a Flutter IDE).
        //   // Notice that the counter didn't reset back to zero; the application
        //   // is not restarted.
        //   primarySwatch: Colors.blue,
        //   primaryColor: Colors.white,
        // ),
        home: FouraScreen(),
      ),
    );
  }
}
